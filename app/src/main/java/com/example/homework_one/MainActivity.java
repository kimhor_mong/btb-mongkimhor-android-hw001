package com.example.homework_one;


import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText fullName;
    private EditText email;
    private EditText password;
    private EditText confirmedPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.btn_save).setOnClickListener(view -> {
            Intent intent = new Intent(view.getContext(), UserDetailActivity.class);

            User user = getUser();

            if(isValidUserDetail(user)){
                intent.putExtra("user", user);
                activityResultLauncher.launch(intent);
            }
        });
    }

    ActivityResultLauncher<Intent> activityResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == Activity.RESULT_OK && result.getData() != null) {
                    clearUserInput();

                    String message = result.getData().getStringExtra("message");
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                }
            }
    );


    private User getUser() {
        fullName = findViewById(R.id.et_full_name);
        email = findViewById(R.id.et_email);
        password = findViewById(R.id.et_password);
        confirmedPassword = findViewById(R.id.et_confirmed_password);

        return new User(fullName.getText().toString(),
                email.getText().toString(),
                password.getText().toString(),
                confirmedPassword.getText().toString());
    }


    private boolean isValidUserDetail(User user){

        boolean isValid = true;

        if(user.getFullName().length() == 0){
            fullName.setError("Full name is required");
            isValid = false;
        }

        if(user.getEmail().length() == 0){
            email.setError("Email is required");
            isValid = false;
        }

        if( user.getEmail().length() != 0  && !isValidEmail(user.getEmail())){
            email.setError("Invalid Email");
            isValid = false;
        }

        if(user.getPassword().length() == 0){
            password.setError("Password is required");
            isValid = false;
        }

        if(user.getConfirmedPassword().length() == 0){
            confirmedPassword.setError("Confirmed Password is required");
            isValid = false;
        }

        if (user.getPassword().length() != 0 && user.getConfirmedPassword().length() != 0){
            if(!user.getPassword().equals(user.getConfirmedPassword())){
                confirmedPassword.setError("Password does not match");
                isValid = false;
            }
        }

        return  isValid;
    }




    private boolean isValidEmail(String emailStr) {
        String pattern ="^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$";
        return emailStr.matches(pattern);
    }

    private void clearUserInput() {
        fullName.setText("");
        email.setText("");
        password.setText("");
        confirmedPassword.setText("");
    }
}