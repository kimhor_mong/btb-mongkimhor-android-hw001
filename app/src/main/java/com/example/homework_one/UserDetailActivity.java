package com.example.homework_one;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class UserDetailActivity extends AppCompatActivity {

    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_detail);

        intent = getIntent();
        User user = (User) intent.getSerializableExtra("user");

        TextView fullName = findViewById(R.id.tv_full_name);
        TextView email = findViewById(R.id.tv_email);
        TextView password = findViewById(R.id.tv_password);

        fullName.setText(user.getFullName());
        email.setText(user.getEmail());
        password.setText(user.getPassword());

        findViewById(R.id.btn_go_back).setOnClickListener(view -> goBack());
    }

    @Override
    public void onBackPressed() {
        goBack();
    }

    private void goBack(){
        intent.putExtra("message", "Please create a new profile again");
        setResult(RESULT_OK, intent);
        finish();
    }
}